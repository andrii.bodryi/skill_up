//
//  CustomViewManager.m
//  skill_up
//
//  Created by User on 26.03.2020.
//  Copyright © 2020 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "React/RCTViewManager.h"


@interface RCT_EXTERN_MODULE(CustomViewManager, RCTViewManager)
RCT_EXPORT_VIEW_PROPERTY(source, NSString)

@end
