//
//  ViewManager.swift
//  skill_up
//
//  Created by User on 26.03.2020.
//  Copyright © 2020 Facebook. All rights reserved.
//

import Foundation

@objc(CustomViewManager)
class CustomViewManager: RCTViewManager {
  override func view() -> UIView! {
    let label = UILabel()
    label.text = "From CustomViewManager"
    label.textAlignment = .center
    return label
  }
}
