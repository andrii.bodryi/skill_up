import React from 'react';
import {requireNativeComponent, StyleSheet} from 'react-native';

const RCTCustomView = requireNativeComponent('CustomView');

export default class CustomView extends React.PureComponent {
  render() {
    return <RCTCustomView {...this.props} />;
  }
}
